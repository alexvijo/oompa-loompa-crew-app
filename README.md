![umpa-loompa](https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/logo-umpa-loompa.png)
# Oompa Loompa Crew App
### Web app to manage the Oompa Loompa's crew of Willy Wonka.

## Installing and run Oompa
```sh
git clone https://alexvijo@bitbucket.org/alexvijo/oompa-loompa-crew-app.git
cd oompa-loompa-crew-app
npm install -g gulp
npm install
npm start

url: http://localhost:3000
(!) In order to fetch AWS API it requires enable cors in Chrome(--disable-web-security --user-data-dir) 
or using extension such 'Allow-Control-Allow-Origin: *'.
```

## For production build
```sh
npm run build
```